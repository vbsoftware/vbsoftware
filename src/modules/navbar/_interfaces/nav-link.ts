export interface NavLink {
    name: string;
    externalLink?: string;
    routerLink?: string;
}
