import Api from '@/services/api';
import { MailgunRequest } from '../_interfaces/mailgun-request';

export default {
    postMessage(request: MailgunRequest) {
        return Api().post('/messages', request);
    },
};
