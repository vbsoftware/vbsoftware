export interface MailgunRequest {
    subject: string;
    content: string;
    to: string;
}
