export interface ContactInfo {
    fullName: string;
    email: string;
    message: string;
}
